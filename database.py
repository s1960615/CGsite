import csv
import pandas as pd
from .GCapp import models

def upload2Database():
    data = pd.read_csv('BoardGameData.csv')
    for id, name, intro, score, img in zip(data['id'], data['name'], data['intro'], data['score'], data['img']):
        models.Games(
            game_id= id,
            game_name= name,
            game_introduction= intro,
            game_score= score,
            game_image= img,
        ).save()
        #print(type(dataList))


upload2Database()