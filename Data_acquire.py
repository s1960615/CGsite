import urllib.request
import requests
from bs4 import BeautifulSoup
import csv
import json
from datetime import datetime
import re
import os
import numpy as np

class DataGrab:

    def generateUrlList(self):
        # specify the url
        quote_page ='https://boardgamegeek.com/browse/boardgame/page/12'

        # query the website and return the html to the variable ‘page’
        page = urllib.request.urlopen(quote_page)

        # parse the html using beautiful soap and store in variable `soup`
        soup = BeautifulSoup(page, 'html.parser')

        # first get the url list and store it into a text file
        urlList = open('test.txt','a')

        for i in range(100):
        # Take out the <div> of name and get its value
            soupObj = soup.find('div', attrs={'id': 'results_objectname' + str(i+1)})
            for link in soupObj.find_all(name='a',attrs={"href":re.compile(r'^/boardgame/')}):
                gameUrl = re.findall('<a href="(.*?)">', str(link))[0]
                urlList.write(gameUrl + '\n')


    # Read from text file
    def readFromFile(self):
        urlList = []
        for line in open('urlList.txt','r'):
            line = line[:-1] # remove \n
            urlList.append(line)
        return urlList

    # Grab data from web pages throught url list
    def dataGrab(self, urls):
        urlNum = len(urls)
        gameID = 1
        with open('newBoardGameData.csv', 'a', encoding='utf-8') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow(["id", "name", "intro", "score","img"])

            for i in range(urlNum):
                quote_page = 'https://boardgamegeek.com/' + str(urls[i])
                page = urllib.request.urlopen(quote_page)
                soup = BeautifulSoup(page, 'html.parser')
                print(soup)
                break


                # get the game name
                soupObj = soup.find('meta', attrs={'name': 'title'})
                gameName = soupObj['content']

                # get the game score
                soupObj = soup.find('script', attrs={'type':'application/ld+json'})
                gameScore = json.loads(soupObj.get_text())["aggregateRating"]['ratingValue']

                #get the game discription
                soupObj = soup.find('meta',attrs={'name':'description'})
                gameIntro = soupObj['content']

                #get the game image and store it
                soupObj = soup.find('script', attrs={'type':'application/ld+json'})
                gameImageLink = json.loads(soupObj.get_text())['image']
                print(gameImageLink)
                imgDir = './GameImages/'
                if not os.path.exists(imgDir):
                    os.makedirs(imgDir)

                try:
                    pic = requests.get(gameImageLink, timeout=10)
                except requests.exceptions.ConnectionError:
                    print('图片无法下载')
                    continue
                    # 保存图片路径
                dir = imgDir + str(gameID) + '.jpg'
                imgName = str(gameID) + '.jpg'
                fp = open(dir, 'wb')
                fp.write(pic.content)
                fp.close()

                # Store those information into a csv file
                writer.writerow([gameID, gameName, gameIntro, gameScore, imgName])
                print(imgDir)
                print(imgName)

                gameID = gameID + 1




object = DataGrab()
urlList = object.readFromFile()
object.dataGrab(urlList)


'''
with open('BoardGameData.csv', 'w' ) as csv_file:
 writer = csv.writer(csv_file)
 writer.writerow([gameName,datetime.now()])
'''
