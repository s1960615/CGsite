# Generated by Django 3.0.3 on 2020-03-31 14:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('GCapp', '0033_auto_20200329_2141'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostComments',
            fields=[
                ('pcomment_id', models.AutoField(primary_key=True, serialize=False)),
                ('pcomment_text', models.TextField()),
                ('comment_likecount', models.IntegerField(default=0)),
                ('pcomment_date', models.DateTimeField(auto_now_add=True)),
                ('pcomment_community', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='GCapp.Community')),
                ('pcomment_publisher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='GCapp.Users')),
            ],
        ),
    ]
