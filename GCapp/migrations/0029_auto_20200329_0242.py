# Generated by Django 3.0.3 on 2020-03-29 01:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GCapp', '0028_gamecollection_collection_game'),
    ]

    operations = [
        migrations.AlterField(
            model_name='games',
            name='game_purchase',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
