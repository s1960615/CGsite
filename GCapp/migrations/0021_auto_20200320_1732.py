# Generated by Django 3.0.3 on 2020-03-20 17:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GCapp', '0020_auto_20200309_1817'),
    ]

    operations = [
        migrations.AlterField(
            model_name='games',
            name='game_type',
            field=models.CharField(choices=[('N/A', 'N/A'), ('Adventure', 'Adventure'), ('Ancient', 'Ancient'), ('Educational', 'Educational'), ('Fighting', 'Fighting'), ('Racing', 'Racing'), ('Sports', 'Sports'), ('Wargame', 'Wargame'), ('Zombies', 'Zombies')], default='N/A', max_length=200),
        ),
        migrations.AlterField(
            model_name='users',
            name='user_tags',
            field=models.CharField(choices=[('N/A', 'N/A'), ('Adventure', 'Adventure'), ('Ancient', 'Ancient'), ('Educational', 'Educational'), ('Fighting', 'Fighting'), ('Racing', 'Racing'), ('Sports', 'Sports'), ('Wargame', 'Wargame'), ('Zombies', 'Zombies')], default='N/A', max_length=200),
        ),
    ]
