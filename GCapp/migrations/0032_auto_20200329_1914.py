# Generated by Django 3.0.3 on 2020-03-29 18:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GCapp', '0031_game_news_news_publisher'),
    ]

    operations = [
        migrations.RenameField(
            model_name='games',
            old_name='game_purchase',
            new_name='game_Amazon',
        ),
        migrations.AddField(
            model_name='games',
            name='game_Ebay',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
