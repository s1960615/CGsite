from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from .models import Game_news
from .models import GameCollection
from .models import Games
from .models import Users
from .models import Comments
from .models import Community
from .models import Posts
from .models import PostComments
from django.template import loader
from django.http import Http404
from . import models
from . import forms
from .forms import CommentForm
from .forms import TypeForm
from .forms import PostCommentForm
import hashlib
import json
import random
import string
import csv
import pandas as pd

#加密算法
def hash_code(s, salt='mysite'):# 加点盐
    h = hashlib.sha256()
    s += salt
    h.update(s.encode())  # update方法只接收bytes类型
    return h.hexdigest()


# Create your views here.
'''
Main page
'''
def index(request):
    #将数据存入数据库
    '''
    data = pd.read_csv('./BoardGameData.csv')
    for id, name, intro, score, img in zip(data['id'], data['name'], data['intro'], data['score'], data['img']):
        typeList = ['N/A',
                   'abstract strategy',
                   'action / dexterity',
                   'adventure',
                   'age of reason',
                   'american civil war',
                   'american indian wars',
                   'american revolutionary war',
                   'american west',
                   'ancient',
                   'animals',
                   'arabian',
                   'aviation / flight',
                   'bluffing',
                   'book',
                   'card game',
                   "children's game",
                   'city building',
                   'civil war',
                   'civilization',
                   'collectible components',
                   'comic book / strip',
                   'deduction',
                   'dice',
                   'economic',
                   'educational',
                   'electronic',
                   'environmental',
                   'expansion for base-game',
                   'exploration',
                   'fan expansion',
                   'fantasy',
                   'farming',
                   'fighting',
                   'game system',
                   'horror',
                   'humor',
                   'industry / manufacturing',
                   'korean war',
                   'mafia',
                   'math',
                   'mature / adult',
                   'maze',
                   'medical',
                   'medieval',
                   'memory',
                   'miniatures',
                   'modern warfare',
                   'movies / tv / radio theme',
                   'murder/mystery',
                   'music',
                   'mythology',
                   'napoleonic',
                   'nautical',
                   'negotiation',
                   'novel-based',
                   'number',
                   'party game',
                   'pike and shot',
                   'pirates',
                   'political',
                   'post-napoleonic',
                   'prehistoric',
                   'print & play',
                   'puzzle',
                   'racing',
                   'real-time',
                   'religious',
                   'renaissance',
                   'science fiction',
                   'space exploration',
                   'spies/secret agents',
                   'sports',
                   'territory building',
                   'trains',
                   'transportation',
                   'travel',
                   'trivia',
                   'video game theme',
                   'vietnam war',
                   'wargame',
                   'word game',
                   'world war i',
                   'world war ii',
                   'zombies'
                   ]
        models.Games(
            game_id=id,
            game_name=name,
            game_introduction=intro,
            game_score=score,
            game_image=img,
            game_type=random.choice(typeList)
        ).save()

    '''
    '''
    录入用户信息
    '''
    '''
    # name list
    nameList = [
        'EMMA',
        'MIA',
        'OLIVIA',
        'SOPHIA',
        'ISABELLA',
        'EMILY',
        'CAMILA',
        'SOFIA',
        'AVA',
        'ABIGAIL',
        'VICTORIA',
        'CHARLOTTE',
        'EVELYN',
        'LUNA',
        'SCARLETT',
        'PENELOPE',
        'AMELIA',
        'ARIA',
        'MILA',
        'ELIZABETH',
        'CHLOE',
        'ZOE',
        'GRACE',
        'SAMANTHA',
        'GENESIS',
        'ALEXA',
        'MADISON',
        'ELLA',
        'AVERY',
        'LEAH',
        'AUDREY',
        'NATALIE',
        'MELANIE',
        'MAYA',
        'BELLA',
        'ZOEY',
        'LAYLA',
        'HARPER',
        'XIMENA',
        'RILEY',
        'LILY',
        'HANNAH',
        'AALIYAH',
        'VIOLET',
        'DELILAH',
        'VALENTINA',
        'AUBREY',
        'ELENA',
        'ARIANA',
        'ALLISON',
        'ELLIE',
        'NATALIA',
        'STELLA',
        'AURORA',
        'HAZEL',
        'ALICE',
        'HAILEY',
        'GIANNA',
        'ANDREA',
        'EMILIA',
        'LEILANI',
        'EVA',
        'SAVANNAH',
        'ADELINE',
        'BROOKLYN',
        'ARIANNA',
        'VALERIA',
        'CLAIRE',
        'ELIANA',
        'SARAH',
        'NAOMI',
        'VALERIE',
        'RUBY',
        'ELEANOR',
        'ATHENA',
        'JADE',
        'KAYLA',
        'MARIA',
        'ALINA',
        'KAYLEE',
        'ARIEL',
        'KATHERINE',
        'LUCY',
        'KIMBERLY',
        'MELODY',
        'JASMINE',
        'ALYSSA',
        'LILIANA',
        'ASHLEY',
        'SOPHIE',
        'ANNA',
        'BRIANNA',
        'ADDISON',
        'EVERLY',
        'ISLA',
        'ALEXANDRA',
        'LILLIAN',
        'ISABEL',
        'DANIELA',
        'MADELINE',
        'MADELYN',
        'IVY',
        'NORA',
        'JULIA',
        'KENNEDY',
        'DAISY',
        'DALEYZA',
        'ISABELLE',
        'NEVAEH',
        'ARYA',
        'ANGELA',
        'QUINN',
        'ALIYAH',
        'IRIS',
        'GISELLE',
        'JOCELYN',
        'NICOLE',
        'JULIANA',
        'MICHELLE',
        'JULIETTE',
        'LEILA',
        'AUTUMN',
        'ANGELINA',
        'LUCIA',
        'VIVIAN',
        'GABRIELLA',
        'ROSE',
        'SKYLAR',
        'CORA',
        'KYLIE',
        'DIANA',
        'KAMILA',
        'ANA',
        'AMAYA',
        'SADIE',
        'BRIELLE',
        'FAITH',
        'VANESSA',
        'MELISSA',
        'SERENITY',
        'MACKENZIE',
        'JESSICA',
        'JULIANNA',
        'EMERY',
        'AUBREE',
        'SELENA',
        'SIENNA',
        'REAGAN',
        'ESMERALDA',
        'NOVA',
        'TAYLOR',
        'JAYLEEN',
        'JOSEPHINE',
        'STEPHANIE',
        'ARABELLA',
        'CAROLINE',
        'PAISLEY',
        'BAILEY',
        'YARETZI',
        'ALESSANDRA',
        'LEIA',
        'CLARA',
        'DESTINY',
        'SARA',
        'JACQUELINE',
        'JULIET',
        'KINSLEY',
        'LYLA',
        'RYLEE',
        'PEYTON',
        'ALEXIS',
        'ANNABELLE',
        'MIRANDA',
        'WILLOW',
        'GENEVIEVE',
        'JENNIFER',
        'SYDNEY',
        'JIMENA',
        'LAUREN',
        'ALONDRA',
        'CATALINA',
        'ELISE',
        'PIPER',
        'FATIMA',
        'GABRIELA',
        'KAIA',
        'ALANA',
        'CATHERINE',
        'SUMMER',
        'JOANNA',
        'AMANDA',
        'ITZEL',
        'FIONA',
        'ANASTASIA',
        'IZABELLA',
        'ALICIA',
        'CECILIA',
        'NORAH',
        'ADALYN',
        'ADALYNN',
        'LYDIA',
        'ADRIANA',
        'CHARLIE',
        'OLIVE',
        'CAMILLA',
        'LIA',
        'PAIGE',
        'DANIELLA',
        'MALIA',
        'RACHEL',
        'RENATA',
        'VIVIENNE',
        'MAKAYLA',
        'REBECCA',
        'REESE',
        'ELOISE',
        'FERNANDA',
        'MARIAH',
        'MARY',
        'JULIETA',
        'EDEN',
        'MARIANA',
        'ADALINE',
        'AYLA',
        'SIERRA',
        'ALISON',
        'ESTHER',
        'JORDYN',
        'JUNE',
        'PRESLEY',
        'VIVIANA',
        'AYLIN',
        'DAHLIA',
        'SAGE',
        'ADELYN',
        'APRIL',
        'ELIZA',
        'SAMARA',
        'HARLEY',
        'KIARA',
        'MYA',
        'SLOANE',
        'AMBER',
        'NOELLE',
        'ANNIE',
        'KHLOE',
        'LAILA',
        'KEIRA',
        'KIRA',
        'REGINA',
        'ELISA',
        'PHOEBE',
        'ZARA',
        'JAYLENE',
        'LILA',
        'LILLY',
        'IVANNA',
        'JAYLA',
        'AMARA',
        'JOY',
        'EVANGELINE',
        'CELESTE',
        'BROOKE',
        'CAMILLE',
        'JULISSA',
        'ALEJANDRA',
        'ARIELLE',
        'CELINE',
        'IRENE',
        'JANE',
        'LONDON',
        'GUADALUPE',
        'JAZMIN',
        'PARKER',
        'ALEXIA',
        'MARGARET',
        'SERENA',
        'DYLAN',
        'GEMMA',
        'LANA',
        'LUCIANA',
        'TEAGAN',
        'FRIDA',
        'LESLIE',
        'NAYELI',
        'NINA',
        'AILEEN',
        'ARIELLA',
        'GALILEA',
        'KATE',
        'KARLA',
        'ROSALIE',
        'TIFFANY',
        'CHELSEA',
        'MELANY',
        'TRINITY',
        'DANIELLE',
        'ELLIANA',
        'MARLEY',
        'BLAKE',
        'ANAHI',
        'CATALEYA',
        'JAYLAH',
        'RUTH',
        'LOLA',
        'MADELEINE',
        'PRISCILLA',
        'ADELINA',
        'JUNIPER',
        'LILAH',
        'CALI',
        'GRACIE',
        'LEXI',
        'DAKOTA',
        'EMERSON',
        'HOPE',
        'JANELLE',
        'MORGAN',
        'ALAINA',
        'BIANCA',
        'BROOKLYNN',
        'CYNTHIA',
        'EMELY',
        'GIA',
        'TALIA',
        'CAROLINA',
        'MARILYN',
        'CLARISSA',
        'HELEN',
        'JAZLYN',
        'MAGGIE',
        'MIKAYLA',
        'AMINA',
        'KAITLYN',
        'ROWAN',
        'VERA',
        'ALEENA',
        'ANAYA',
        'HAYDEN',
        'KENDRA',
        'SARAI',
        'SASHA',
        'ADELYNN',
        'ANGELICA',
        'KARINA',
        'VERONICA',
        'ARIAH',
        'ALAYNA',
        'DANNA',
        'ELIANNA',
        'FINLEY',
        'MOLLY',
        'NOEMI',
        'CASSIDY',
        'LAURA',
        'RAELYNN',
        'ALEXANDRIA',
        'JORDAN',
        'MEGAN',
        'KAREN',
        'ADA',
        'AITANA',
        'AMIRA',
        'HADLEY',
        'ALLYSON',
        'BRIANA',
        'CHRISTINA',
        'HARLOW',
        'LIANA',
        'ELAINE',
        'GEORGIA',
        'KATELYN',
        'MAIA',
        'TESSA',
        'EILEEN',
        'KASSANDRA',
        'SABRINA',
        'ELLE',
        'HARMONY',
        'MELINA',
        'LILLIANA',
        'LOGAN',
        'RIVER',
        'SIENA',
        'BELEN',
        'CARMEN',
        'JOLENE',
        'LENA',
        'PAYTON',
        'SCARLET',
        'AMELIE',
        'HEIDI',
        'JOSIE',
        'ANGEL',
        'BETHANY',
        'MARGOT',
        'PHOENIX',
        'CALLIE',
        'THEA',
        'CASSANDRA',
        'JULIE',
        'ALANI',
        'EVIE',
        'FRANCESCA',
        'HELENA',
        'DAPHNE',
        'MIRIAM',
        'RYLIE',
        'ALEAH',
        'AVIANA',
        'CRYSTAL',
        'JAMIE',
        'ANGIE',
        'ANIYAH',
        'COLETTE',
        'DAYANA',
        'ERIN',
        'JAZMINE',
        'LEYLA',
        'LUCILLE',
        'RAEGAN',
        'HALEY',
        'KELLY',
        'MADDISON',
        'DELANEY',
        'GLORIA',
        'KATIE',
        'MCKENZIE',
        'REMI',
        'SAWYER',
        'TATIANA',
        'ABBY',
        'ALMA',
        'FREYA',
        'HANNA',
        'MIRA',
        'KENDALL',
        'REYNA',
        'ANNABELLA',
        'KALI',
        'KAYLANI',
        'ROSEMARY',
        'ADELAIDE',
        'BRIELLA',
        'KAILANI',
        'OPHELIA',
        'SELAH',
        'AIMEE',
        'HAVEN',
        'HEAVEN',
        'KENIA',
        'MONICA',
        'ALESSIA',
        'AMIYAH',
        'ANNALISE',
        'DALARY',
        'DULCE',
        'GABRIELLE',
        'JESSIE',
        'NADIA',
        'FELICITY',
        'MYLA',
        'ARELY',
        'ITZAYANA',
        'MAKENZIE',
        'NIA',
        'PALOMA',
        'LINDA',
        'NATHALIE',
        'YARELI',
        'PAOLA',
        'AYLEEN',
        'KENZIE',
        'EVE',
        'ROSIE',
        'SKYE',
        'AVERIE',
        'ELSIE',
        'LONDYN',
        'MABEL',
        'MALIYAH',
        'RYAN',
        'ALLIE',
        'ANGELIQUE',
        'AUDRINA',
        'EMBER',
        'MARISSA',
        'MATILDA',
        'MYRA',
        'ROSELYN',
        'SKYLER',
        'ALYSON',
        'BRENDA',
        'CADENCE',
        'ELSA',
        'GIULIANA',
        'JENNA',
        'MARYAM',
        'JOHANNA']
    # tag list
    tagList = ['N/A',
               'abstract strategy',
               'action / dexterity',
               'adventure',
               'age of reason',
               'american civil war',
               'american indian wars',
               'american revolutionary war',
               'american west',
               'ancient',
               'animals',
               'arabian',
               'aviation / flight',
               'bluffing',
               'book',
               'card game',
               "children's game",
               'city building',
               'civil war',
               'civilization',
               'collectible components',
               'comic book / strip',
               'deduction',
               'dice',
               'economic',
               'educational',
               'electronic',
               'environmental',
               'expansion for base-game',
               'exploration',
               'fan expansion',
               'fantasy',
               'farming',
               'fighting',
               'game system',
               'horror',
               'humor',
               'industry / manufacturing',
               'korean war',
               'mafia',
               'math',
               'mature / adult',
               'maze',
               'medical',
               'medieval',
               'memory',
               'miniatures',
               'modern warfare',
               'movies / tv / radio theme',
               'murder/mystery',
               'music',
               'mythology',
               'napoleonic',
               'nautical',
               'negotiation',
               'novel-based',
               'number',
               'party game',
               'pike and shot',
               'pirates',
               'political',
               'post-napoleonic',
               'prehistoric',
               'print & play',
               'puzzle',
               'racing',
               'real-time',
               'religious',
               'renaissance',
               'science fiction',
               'space exploration',
               'spies/secret agents',
               'sports',
               'territory building',
               'trains',
               'transportation',
               'travel',
               'trivia',
               'video game theme',
               'vietnam war',
               'wargame',
               'word game',
               'world war i',
               'world war ii',
               'zombies'
               ]
    # commmunity list
    communityList = ['N/A', 'gamechat', 'gamequestions', 'bugs', 'find users', 'avatars', 'stats']

    for name in nameList:
        user_name = name.lower().capitalize()
        p1 = random.random()
        if p1 >= 0.5:
            user_gender = 'Male'
        else:
            user_gender = 'Female'
        user_tags = random.choice(tagList)
        user_password = '12345678'
        email_str = ''.join(random.sample(string.ascii_letters + string.digits, 12))
        user_email = email_str + '@gmail.com'
        user_community = random.choice(communityList)
        models.Users(
            user_name = user_name,
            user_password = hash_code(user_password),
            user_email = user_email,
            user_tags = user_tags,
            user_community = user_community,
            user_gender = user_gender,
        ).save()
    '''

    #显示最新的4个新闻和评分最高的游戏
    latest_news_list = Game_news.objects.order_by('-news_date')[:4]
    recommend_game_list = Games.objects.order_by('-game_score')[:12]
    community_list = Community.objects.all()
    template = loader.get_template('GCapp/index.html') #载入模板
    #尝试获取用户的Id, 如果获取到就不用显示Log in
    user_id = request.session.get('user_id', None)
    context = {
         'latest_news_list': latest_news_list,
         'recommend_game_list': recommend_game_list,
         'community_list':community_list,
         'user_id':user_id,
     }#传入上下文
    return HttpResponse(template.render(context, request))

'''
If the corresponding news of id is not existing, then throw out 404 excepton 
'''
def detail(request, news_id):
    try:
        news = Game_news.objects.get(pk=news_id)
    except Game_news.DoesNotExist:
        raise Http404("News does not exist")
    return render(request, 'GCapp/detail.html', {'news': news})

'''
login
'''
def login(request):
    if request.session.get('is_login', None):  # not allowed for duplicated log in
        return redirect('/GCapp/main')
    if request.method == "POST":
        login_form = forms.UserForm(request.POST)
        message = 'Please check the input content'

        if login_form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')
            try:
                user = models.Users.objects.get(user_name=username)
            except:
                message = 'User is not exist!'
                return render(request, 'GCapp/login.html', locals())

            if user.user_password == hash_code(password):
                request.session['is_login'] = True
                request.session['user_id'] = user.user_id
                request.session['user_name'] = user.user_name
                return redirect('/GCapp/main')
            else:
                message = 'The input password is wrong, please input again!'
                return render(request, 'GCapp/login.html',locals())

        else:
            return render(request, 'GCapp/login.html', locals())

    login_form = forms.UserForm()
    return render(request, 'GCapp/login.html',locals())

'''
register
'''
def register(request):
    if request.session.get('is_login', None):
        return redirect('GCapp/main/')

    if request.method == 'POST':
        register_form = forms.RegisterForm(request.POST)
        message = "Please check the input content！"
        if register_form.is_valid():
            username = register_form.cleaned_data.get('username')
            password1 = register_form.cleaned_data.get('password1')
            password2 = register_form.cleaned_data.get('password2')
            email = register_form.cleaned_data.get('email')
            gender = register_form.cleaned_data.get('gender')
            tag = register_form.cleaned_data.get('tag')
            community = register_form.cleaned_data.get('community')

            if password1 != password2:
                message = 'The passwords entered are not same!'
                return render(request, 'GCapp/register.html', locals())
            else:
                same_name_user = models.Users.objects.filter(user_name=username)
                if same_name_user:
                    message = 'User exists'
                    return render(request, 'GCapp/register.html', locals())
                same_email_user = models.Users.objects.filter(user_email=email)
                if same_email_user:
                    message = 'Email has been registered'
                    return render(request, 'GCapp/register.html', locals())

                new_user = models.Users()
                new_user.user_name = username
                new_user.user_password = hash_code(password1)
                new_user.user_email = email
                new_user.user_gender = gender
                new_user.user_tags = tag
                new_user.user_community = community
                new_user.save()

                return redirect('/GCapp/login/')
        else:
            return render(request, 'GCapp/register.html', locals())
    register_form = forms.RegisterForm()
    return render(request, 'GCapp/register.html', locals())

'''
logout
'''
def logout(request):
    if not request.session.get('is_login', None):
        # 如果本来就未登录，也就没有登出一说
        return redirect("/GCapp/login/")
    request.session.flush()
    # 或者使用下面的方法
    # del request.session['is_login']
    # del request.session['user_id']
    # del request.session['user_name']
    return redirect('/GCapp/index')

'''
main page
'''
def main(request):
    userid = request.session.get('user_id', None)
    game_collection_list = GameCollection.objects.filter(collection_owner = userid)
    tag = Users.objects.get(user_id= userid)#获取用户标签
    recommend_game_list = Games.objects.filter(game_type=tag.user_tags)
    filtered_game_list = []
    '''
    View game collection by choosing types
    '''
    if request.method == 'POST':
        type_form = TypeForm(request.POST)

        type_value = request.POST.get('type', "")  # 这里可以取到下拉表单中的值
        print(type_value)
        '''
        Search from the game collection table and filter games meet the type requirement
        '''
        list = GameCollection.objects.filter(collection_owner = userid)
        for game in game_collection_list:
            gameType = game.collection_gameid.game_type
            if gameType == type_value.lower():
                    filtered_game_list.append(game)
            else:
                    pass
        print(filtered_game_list)
            # 接下来就是保存数值与其他逻辑了
    type_form = TypeForm()
    context = {'game_collection_list': game_collection_list,
               'recommend_game_list': recommend_game_list,
               'type_form': type_form,
               'filtered_game_list': filtered_game_list}#传入上下文

    return render(request, 'GCapp/main.html', context)

def game(request, collection_gameid):
    try:
        game = Games.objects.get(game_id=collection_gameid)

        if request.method == 'POST':
            # 用户提交的数据存在 request.POST 中，这是一个类字典对象。
            # 我们利用这些数据构造了 CommentForm 的实例，这样 Django 的表单就生成了。
            form = CommentForm(request.POST)

            # 当调用 form.is_valid() 方法时，Django 自动帮我们检查表单的数据是否符合格式要求。
            if form.is_valid():
                # 检查到数据是合法的，调用表单的 save 方法保存数据到数据库，
                # commit=False 的作用是仅仅利用表单的数据生成 Comment 模型类的实例，但还不保存评论数据到数据库。
                comment = form.save(commit=False)
                userid = request.session.get('user_id', None)
                username = Users.objects.get(user_id=userid)

                # 将评论和被评论的游戏关联起来。
                comment.comment_game = game
                comment.comment_publisher = username

                # 最终将评论数据保存进数据库，调用模型实例的 save 方法
                comment.save()

                # 重定向到 game 的详情页，实际上当 redirect 函数接收一个模型的实例时，它会调用这个模型实例的 get_absolute_url 方法，
                # 然后重定向到 get_absolute_url 方法返回的 URL。

                #return redirect(game)

            else:
                # 检查到数据不合法，重新渲染详情页，并且渲染表单的错误。
                # 因此我们传了三个模板变量给 game.html，
                # 一个是游戏（game），一个是评论列表，一个是表单 form
                # 注意这里我们用到了 game.comment_set.all() 方法，
                # 这个用法有点类似于 game.objects.all()
                # 其作用是获取这个 game 下的的全部评论，
                # 因为 Games 和 Comment 是 ForeignKey 关联的，
                # 因此使用 game.comment_set.all() 反向查询全部评论。
                # 具体请看下面的讲解。
                comment_list = Comments.objects.filter(comment_game=game)
                context = {'game': game,
                           'form': form,
                           'comment_list': comment_list
                           }
                return render(request, 'GCapp/game.html', context=context)

        # 记得在顶部导入 CommentForm
        form = CommentForm()
        # 获取这篇 game 下的全部评论
        comment_list = Comments.objects.filter(comment_game = game)

        # 将游戏、表单、以及游戏下的评论列表作为模板变量传给 game.html 模板，以便渲染相应数据。
        context = {'game': game,
                   'form': form,
                  'comment_list': comment_list
                   }
        return render(request, 'GCapp/game.html', context=context)
    except Games.DoesNotExist:
        raise Http404("Game does not exist")
    return render(request, 'GCapp/game.html', {'game': game})

def search(request):
    q = request.POST['gamename']
    print(q)
    error_msg = ''

    if not q:
        error_msg = 'Please input validate key word'
        return render(request, 'GCapp/errors.html', {'error_msg': error_msg})
    game_list = Games.objects.filter(game_name__icontains = q)
  
    context = {'error_msg': error_msg, 'game_list': game_list, 'search_game':q}
    return render(request, 'GCapp/search.html', context)

def searchMygame(request):
    q = request.POST['gamename']
    print(q)
    error_msg = ''

    if not q:
        error_msg = '请输入关键词'
        return render(request, 'GCapp/errors.html', {'error_msg': error_msg})
    userid = request.session.get('user_id', None)
    owner = Users.objects.get(user_id=userid)
    game_list = GameCollection.objects.filter(collection_owner = owner).filter(collection_game__icontains = q)
    context = {'error_msg': error_msg, 'game_list': game_list,'search_game':q}
    return render(request, 'GCapp/searchMygame.html', context)


def error(request):
    pass
    return render(request, 'GCapp/errors.html')

def delete(request):
    gid = request.POST['game_id']
    print(gid)
    GameCollection.objects.get(collection_gameid=gid).delete()
    return HttpResponse(json.dumps({'state': 'SUCCESS'}))

def add(request):
    gameid = request.POST['game_id']#获取gameid
    gameobj = Games.objects.get(game_id=gameid)#利用gameid获取外键对象
    userid = request.session.get('user_id', None)#获取userid
    owner = Users.objects.get(user_id=userid)#利用userid查找用户名字
    game_name = Games.objects.get(game_id= gameid)#利用gameid获取游戏名字
    #查找用户拥有的collection,避免add相同的游戏
    if (GameCollection.objects.filter(collection_owner=owner).filter(collection_gameid=gameid).exists()):
        print('same game!')
        return HttpResponse(json.dumps({'state': 'SUCCESS'}))
    else:
        collection = GameCollection(collection_owner = owner, collection_gameid= gameobj, collection_game=game_name)#外键以对象方式插入
        collection.save()  # flush到数据库中
        return HttpResponse(json.dumps({'state': 'SUCCESS'}))

def community(request, communityid):
    try:
        user_id = request.session.get('user_id', None)  # 获取userid
        community = Community.objects.get(community_id=communityid)
        #利用community找到所有属于这个社区的帖子
        post_list = Posts.objects.filter(post_community = community.community_id)
        return render(request, 'GCapp/community.html' ,{'community': community,'post_list':post_list,'user_id':user_id})
    except Community.DoesNotExist:
        raise Http404("Community does not exist")
    #return render(request, 'GCapp/community.html.html', {'game': game})

def post(request,postid):
    try:
        post = Posts.objects.get(post_id=postid)
        pcommunity = post.post_community
        user_id = request.session.get('user_id', None)
        if request.method == 'POST':
            # 用户提交的数据存在 request.POST 中，这是一个类字典对象。
            # 我们利用这些数据构造了 CommentForm 的实例，这样 Django 的表单就生成了。
            form = PostCommentForm(request.POST)

            # 当调用 form.is_valid() 方法时，Django 自动帮我们检查表单的数据是否符合格式要求。
            if form.is_valid():
                # 检查到数据是合法的，调用表单的 save 方法保存数据到数据库，
                # commit=False 的作用是仅仅利用表单的数据生成 Comment 模型类的实例，但还不保存评论数据到数据库。
                pcomment = form.save(commit=False)
                userid = request.session.get('user_id', None)
                username = Users.objects.get(user_id=userid)

                # 将评论和被评论的游戏关联起来。
                pcomment.pcomment_community = pcommunity
                pcomment.pcomment_publisher = username

                # 最终将评论数据保存进数据库，调用模型实例的 save 方法
                pcomment.save()

                # 重定向到 game 的详情页，实际上当 redirect 函数接收一个模型的实例时，它会调用这个模型实例的 get_absolute_url 方法，
                # 然后重定向到 get_absolute_url 方法返回的 URL。

                #return redirect(game)

            else:
                # 检查到数据不合法，重新渲染详情页，并且渲染表单的错误。
                # 因此我们传了三个模板变量给 game.html，
                # 一个是游戏（game），一个是评论列表，一个是表单 form
                # 注意这里我们用到了 game.comment_set.all() 方法，
                # 这个用法有点类似于 game.objects.all()
                # 其作用是获取这个 game 下的的全部评论，
                # 因为 Games 和 Comment 是 ForeignKey 关联的，
                # 因此使用 game.comment_set.all() 反向查询全部评论。
                # 具体请看下面的讲解。
                post_comment_list = PostComments.objects.filter(pcomment_community = pcommunity)
                context = {'pcommunity': pcommunity,
                           'form': form,
                           'post_comment_list': post_comment_list,
                           'user_id': user_id
                           }
                return render(request, 'GCapp/post.html', context=context)

        # 记得在顶部导入 CommentForm
        form = PostCommentForm()
        # 获取这篇 game 下的全部评论
        post_comment_list = PostComments.objects.filter(pcomment_community = pcommunity)

        # 将游戏、表单、以及游戏下的评论列表作为模板变量传给 game.html 模板，以便渲染相应数据。
        context = {'post': post,
                   'form': form,
                   'post_comment_list': post_comment_list,
                   'user_id':user_id
                   }
        return render(request, 'GCapp/post.html', context)
    except Posts.DoesNotExist:
        raise Http404("Post does not exist!")
    return render(request, 'GCapp/post.html')

def profile(request):
    userid = request.session.get('user_id', None)
    user = Users.objects.get(user_id=userid)
    context = {'user': user}
    return render(request, 'GCapp/profile.html', context)

def edit(request):
    if request.method == 'POST':
        edit_form = forms.EditForm(request.POST)
        message = "Please check the input content！"
        if edit_form.is_valid():
            username = edit_form.cleaned_data.get('username')
            password1 = edit_form.cleaned_data.get('password1')
            password2 = edit_form.cleaned_data.get('password2')
            email = edit_form.cleaned_data.get('email')
            gender = edit_form.cleaned_data.get('gender')
            tag = edit_form.cleaned_data.get('tag')
            community = edit_form.cleaned_data.get('community')

            if password1 != password2:
                message = 'The passwords entered are not same!'
                return render(request, 'GCapp/edit.html', locals())
            else:
                '''
                Although users may change their informaiton, their user id won't change so we use them to identify each user
                '''
                userid = request.session.get('user_id', None)
                Users.objects.filter(user_id=userid).update(user_name = username,
                                                            user_password = hash_code(password1),
                                                            user_email = email,
                                                            user_gender = gender,
                                                            user_tags = tag,
                                                            user_community = community)

                return redirect('/GCapp/profile/')
        else:
            return render(request, 'GCapp/edit.html', locals())
    edit_form = forms.EditForm()
    return render(request, 'GCapp/edit.html',locals())
