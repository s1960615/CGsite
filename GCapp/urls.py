from django.urls import path
from django.urls import include
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = 'GCapp'
urlpatterns = [
    path('index/', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logout, name = 'logout'),
    path('main/', views.main, name = 'main'),
    path('<int:collection_gameid>/',views.game, name = 'game'),
    path('news/<int:news_id>/', views.detail, name='detail'),
    path('errors/', views.error, name = 'errors'),
    path('search/', views.search ,name = 'search'),
    path('searchMygame/', views.searchMygame ,name = 'searchMygame'),
    path('delete/', views.delete, name = 'delete'),
    path('add/', views.add, name = 'add'),
    path('community/<int:communityid>/', views.community, name = 'community'),
    path('post/<int:postid>/',views.post,name = 'post'),
    path('profile/', views.profile, name = 'profile'),
    path('edit/',views.edit, name = 'edit'),
    #path('captcha/', include('captcha.urls'))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)